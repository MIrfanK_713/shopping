var express = require('express');
const productHelpers = require('../helpers/product-helpers');
var router = express.Router();
var productHelper = require('../helpers/product-helpers')
    /* GET users listing. */
router.get('/', function(req, res, next) {
    let products = [{

            name: "I phone 11",
            category: "mobile",
            description: "This is a good phone",
            image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQnCBWrUV5GWqv4ipsjynX_BuO--3SF57VmqA&usqp=CAU"

        },

        {

            name: "Samsung",
            category: "mobile",
            description: "This is a good phone",
            image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRH-PuJXr6L21BnRw_SANTT75jheVwda4jRyY7OUDAO6XYy0dAWIUeGuC89Cra6vs3e180&usqp=CAU"

        },

        {

            name: "Realme",
            category: "mobile",
            description: "This is a good phone",
            image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQnCBWrUV5GWqv4ipsjynX_BuO--3SF57VmqA&usqp=CAU"

        },

        {

            name: "Oppo",
            category: "mobile",
            description: "This is a good phone",
            image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQnCBWrUV5GWqv4ipsjynX_BuO--3SF57VmqA&usqp=CAU"

        }
    ]
    res.render('admin/view-products', { admin: true, products })
});
router.get('/add-product', function(req, res) {
    res.render('admin/add-product')
})
router.post('/add-product', (req, res) => {
    console.log(req.body)
    console.log(req.files.Image);

    productHelpers.addProduct(req.body, (result) => {
        res.render("admin/add-product")
    })

})
module.exports = router;